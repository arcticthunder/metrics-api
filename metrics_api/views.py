from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework_extensions.mixins import NestedViewSetMixin

from .models import Project, Metric
from .serializers import ProjectSerializer, MetricSerializer, UserSerializer


class ProjectViewSet(NestedViewSetMixin, viewsets.ModelViewSet):
    serializer_class = ProjectSerializer

    def get_queryset(self):
        user = self.request.user
        return Project.objects.filter(owner=user)
    
    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

class MetricViewSet(NestedViewSetMixin, viewsets.ModelViewSet):
    serializer_class = MetricSerializer
    
    @action(detail=False, url_path='upload', methods=['post'])
    def bulk_upload(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, many=True)
        if serializer.is_valid(raise_exception=True):
            id = self.request.path.split('/')[3]
            project= Project.objects.get(pk=id)
            if project:
                serializer.save(project_id=project)

        return Response("Upload Complete!")

    def get_queryset(self):
        user = self.request.user
        id = self.request.path.split('/')[3]
        project= Project.objects.get(pk=id)
        return Metric.objects.filter(project_id=project).filter(project_id__owner=user)

    def perform_create(self, serializer):
        id = self.request.path.split('/')
        project = Project.objects.get(pk=id[3])
        if project:
            serializer.save(project_id=project)


class UserCreate(viewsets.ModelViewSet):
    authentication_classes = ()
    permission_classes = ()
    serializer_class = UserSerializer
